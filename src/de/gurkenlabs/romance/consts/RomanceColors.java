package de.gurkenlabs.romance.consts;

import java.awt.Color;

public class RomanceColors {
  private RomanceColors() {}

  public static final Color TEXT_DARK = Color.DARK_GRAY;
  public static final Color TEXT_BRIGHT = new Color(240, 240, 252);
  public static final Color TEXT_OUTLINE = new Color(81, 90, 92);
  public static final Color SHADOW = new Color(0, 0, 0, 50);
  public static final Color TRIGGER_BORDER = new Color(255, 209, 90);
}
