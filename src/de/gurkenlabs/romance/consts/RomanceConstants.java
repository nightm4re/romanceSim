package de.gurkenlabs.romance.consts;

import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.util.Imaging;
import java.awt.Font;
import java.awt.image.BufferedImage;

public final class RomanceConstants {
  private RomanceConstants() {}

  public static final int fade = 4000;
  public static final int interact_delay = 3000;
  public static final int smooch_delay = 2000;
  public static final Font UI_FONT = Resources.fonts().get("Janitor.ttf", 35f);
  public static final BufferedImage INTERACT_PROMPT =
      Imaging.scale(Resources.images().get("interactPrompt.png"), 3f);
}
