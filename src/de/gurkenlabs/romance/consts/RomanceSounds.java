package de.gurkenlabs.romance.consts;

import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.sound.Sound;
import de.gurkenlabs.romance.Objective;

public final class RomanceSounds {
  private RomanceSounds() {}

  public static Sound[] smoochSounds =
      new Sound[] {
        Resources.sounds().get("smooch1"),
        Resources.sounds().get("smooch2"),
        Resources.sounds().get("smooch3"),
        Resources.sounds().get("smooch4"),
        Resources.sounds().get("smooch5"),
        Resources.sounds().get("smooch6"),
        Resources.sounds().get("smooch7"),
        Resources.sounds().get("smooch8"),
        Resources.sounds().get("smooch9"),
        Resources.sounds().get("smooch10")
      };
  public static Sound[] attentionSounds =
      new Sound[] {
        Resources.sounds().get("attention1"),
        Resources.sounds().get("attention2"),
        Resources.sounds().get("attention3"),
        Resources.sounds().get("attention4"),
        Resources.sounds().get("attention5"),
        Resources.sounds().get("attention6"),
        Resources.sounds().get("attention7"),
        Resources.sounds().get("attention8"),
        Resources.sounds().get("attention9"),
        Resources.sounds().get("attention10"),
        Resources.sounds().get("attention11"),
        Resources.sounds().get("attention12"),
        Resources.sounds().get("attention13"),
        Resources.sounds().get("attention14")
      };
  public static Sound[] fulfillSounds =
      new Sound[] {
        Resources.sounds().get("fulfill1"),
        Resources.sounds().get("fulfill2"),
        Resources.sounds().get("fulfill3"),
        Resources.sounds().get("fulfill4"),
        Resources.sounds().get("fulfill5"),
        Resources.sounds().get("fulfill6"),
        Resources.sounds().get("fulfill7"),
        Resources.sounds().get("fulfill8"),
        Resources.sounds().get("fulfill9"),
        Resources.sounds().get("fulfill10"),
        Resources.sounds().get("fulfill11")
      };

  public static Sound getInteractionSound(Objective obj) {
    switch (obj) {
      case MAKEFIRE:
        return Resources.sounds().get("makeFire");
      case GETWINE:
        return Resources.sounds().get("getWine");
      case SERVEWINE:
        return Resources.sounds().get("serveWine");
      case TURNONTV:
        return Resources.sounds().get("turnOnTV");
      case COOK:
        return Resources.sounds().get("cook");
      case SETTABLE:
        return Resources.sounds().get("setTable");
      default:
        return null;
    }
  }

  public static final Sound backgroundMusic = Resources.sounds().get("backgroundMusic");
}
