package de.gurkenlabs.romance;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.IEntity;
import de.gurkenlabs.litiengine.entities.LightSource;
import de.gurkenlabs.litiengine.entities.SoundSource;
import de.gurkenlabs.litiengine.entities.Spawnpoint;
import de.gurkenlabs.litiengine.entities.Trigger;
import de.gurkenlabs.litiengine.graphics.PositionLockCamera;
import de.gurkenlabs.litiengine.graphics.emitters.Emitter;
import de.gurkenlabs.litiengine.input.Input;
import de.gurkenlabs.romance.consts.RomanceConstants;
import de.gurkenlabs.romance.consts.RomanceSounds;
import de.gurkenlabs.romance.entities.Matze;
import de.gurkenlabs.romance.entities.Monna;
import java.awt.event.KeyEvent;

public final class GameManager {

  private static int currentObjective;
  private static final Objective[] objectiveOrder =
      new Objective[] {
        Objective.MAKEFIRE,
        Objective.KISS,
        Objective.GETWINE,
        Objective.SERVEWINE,
        Objective.KISS,
        Objective.TURNONTV,
        Objective.KISS,
        Objective.COOK,
        Objective.KISS,
        Objective.SETTABLE,
        Objective.KISS,
        Objective.FINISHED
      };

  private GameManager() {}

  public static Objective getObjective() {
    return objectiveOrder[currentObjective];
  }

  public static void nextObjective() {
    currentObjective =
        currentObjective == objectiveOrder.length - 1 ? currentObjective : currentObjective + 1;
    if (getObjective() == Objective.FINISHED) {
      finishGame();
    }
  }

  public static void init() {
    Game.audio().playMusic(RomanceSounds.backgroundMusic);
    Game.audio().setMaxDistance(200);
    Game.world()
        .onLoaded(
            env -> {
              Spawnpoint spawnMatze = env.getSpawnpoint("spawn-matze");
              Spawnpoint spawnMonna = env.getSpawnpoint("spawn-monna");
              spawnMatze.spawn(Matze.instance());
              spawnMonna.spawn(Monna.instance());
              Matze.instance().walkAroundLikeMotherfucker();
              env.getTriggers()
                  .forEach(
                      t -> {
                        updateTriggerTargets(t, false);
                        t.addActivatedListener(
                            l -> {
                              performInteraction(t, getObjective());
                            });
                      });

              Game.graphics().setBaseRenderScale(6f);
              Game.world().setCamera(new PositionLockCamera(Matze.instance()));
              Game.world().camera().setClampToMap(true);
            });
    Input.keyboard()
        .onKeyTyped(
            KeyEvent.VK_F,
            k -> {
              Trigger activeTrigger = Game.world().environment().getTrigger(getObjective().name());
              if (activeTrigger == null || !activeTrigger.canTrigger(Matze.instance())) {
                return;
              }
              activeTrigger.interact(Matze.instance());
            });
    Input.keyboard()
        .onKeyTyped(
            KeyEvent.VK_F1,
            k -> {
              Game.audio().stopMusic();
              Game.audio().playMusic(RomanceSounds.backgroundMusic);
            });
  }

  private static void finishGame() {
    Game.audio().fadeMusic(RomanceConstants.interact_delay + RomanceConstants.fade);
    Game.window()
        .getRenderComponent()
        .fadeOut(RomanceConstants.interact_delay + RomanceConstants.fade);
    Game.loop()
        .perform(RomanceConstants.interact_delay + RomanceConstants.fade, () -> System.exit(0));
  }

  private static void performInteraction(Trigger t, Objective objective) {
    if (objective == Objective.KISS) {
      Matze.instance().smooch();
      Game.loop()
          .perform(
              RomanceConstants.smooch_delay,
              () -> {
                Matze.instance().walkAroundLikeMotherfucker();
                Monna.instance().idle();
                if (t.getName().equals(getObjective().name())) {
                  nextObjective();
                }
              });
      return;
    }
    Matze.instance().interact();
    Game.loop()
        .perform(
            RomanceConstants.interact_delay,
            () -> {
              Matze.instance().walkAroundLikeMotherfucker();
              Monna.instance().wave(objective);
              updateTriggerTargets(t, true);
              if (t.getName().equals(getObjective().name())) {
                nextObjective();
              }
            });
  }

  private static void updateTriggerTargets(Trigger t, boolean visible) {
    t.getTargets()
        .forEach(
            target -> {
              IEntity entity = Game.world().environment().get(target);
              if (entity == null) {
                return;
              } else if (entity instanceof LightSource) {
                LightSource l = (LightSource) entity;
                if (visible) {
                  l.activate();
                } else {
                  l.deactivate();
                }
              } else if (entity instanceof SoundSource) {
                SoundSource s = (SoundSource) entity;
                if (visible) {
                  s.play();
                } else {
                  s.stop();
                }
              } else if (entity instanceof Emitter) {
                Emitter e = (Emitter) entity;
                if (visible) {
                  e.activate();
                } else {
                  e.deactivate();
                }
              }
              entity.setVisible(visible);
            });
  }
}
