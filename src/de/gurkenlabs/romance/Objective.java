package de.gurkenlabs.romance;

public enum Objective {
  MAKEFIRE("Ich sollte uns ein kuschliges Kaminfeuer anmachen."),
  GETWINE("Mal sehen, was wir so zu trinken haben..."),
  SERVEWINE("Sie will sicher ein Glas Wein."),
  TURNONTV("Mein Zuckermops braucht Entertainment!"),
  COOK("Zeit, das Abendessen vorzubereiten."),
  SETTABLE("Noch schnell den Tisch gedeckt."),
  KISS("Schon lang keinen Kuss mehr bekommen..."),
  FINISHED("Jetzt wird geschlemmt!");

  private final String description;

  Objective(String s) {
    this.description = s;
  }

  public String getDescription() {
    return description;
  }
}
