package de.gurkenlabs.romance;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.romance.ui.IngameScreen;

public class Program {
  /**
   * The main entry point for the Game.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    // set meta information about the game
    Game.info().setName("Der Jahrestag");
    Game.info().setSubTitle("Sei mal wieder ein bisschen romantisch!");
    Game.info().setVersion("v1.0.0");

    // init the game infrastructure
    Game.init(args);

    Resources.load("game.litidata");
    Game.screens().add(IngameScreen.instance());

    GameManager.init();
    Game.world().loadEnvironment("livingRoom");
    Game.start();
  }
}
