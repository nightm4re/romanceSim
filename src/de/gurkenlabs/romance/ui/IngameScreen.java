package de.gurkenlabs.romance.ui;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.Trigger;
import de.gurkenlabs.litiengine.graphics.TextRenderer;
import de.gurkenlabs.litiengine.gui.screens.GameScreen;
import de.gurkenlabs.romance.GameManager;
import de.gurkenlabs.romance.consts.RomanceColors;
import de.gurkenlabs.romance.consts.RomanceConstants;
import de.gurkenlabs.romance.entities.Matze;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class IngameScreen extends GameScreen {

  private static IngameScreen instance;

  private IngameScreen() {
    super("INGAME");
    setForwardMouseEvents(false);
  }

  public static IngameScreen instance() {
    if (instance == null) {
      instance = new IngameScreen();
    }

    return instance;
  }

  @Override
  public void render(Graphics2D g) {
    super.render(g);
    String instruction = GameManager.getObjective().getDescription();
    double width = g.getFontMetrics().stringWidth(instruction);
    g.setFont(RomanceConstants.UI_FONT);
    g.setColor(RomanceColors.TEXT_BRIGHT);
    TextRenderer.renderWithOutline(
        g,
        instruction,
        Game.window().getWidth() / 2d - width / 2d,
        Game.window().getHeight() * 1 / 20d,
        RomanceColors.TEXT_OUTLINE,
        2f,
        true);

    Trigger t = Game.world().environment().getTrigger(GameManager.getObjective().name());
    if (t == null
        || Game.time().since(Matze.instance().getLastInteract()) < RomanceConstants.interact_delay
        || Game.time().since(Matze.instance().getLastSmooch()) < RomanceConstants.smooch_delay) {
      return;
    }
    g.setColor(updateTriggerColor());
    Game.graphics().renderOutline(g, t.getBoundingBox(), new BasicStroke(3f));
    BufferedImage prompt = RomanceConstants.INTERACT_PROMPT;
    Game.graphics()
        .renderImage(
            g,
            prompt,
            t.getCenter().getX() - prompt.getWidth() / 2d / Game.graphics().getBaseRenderScale(),
            t.getY() - prompt.getHeight() / 2d / Game.graphics().getBaseRenderScale());
  }

  private Color updateTriggerColor() {
    float[] hsb =
        Color.RGBtoHSB(
            RomanceColors.TRIGGER_BORDER.getRed(),
            RomanceColors.TRIGGER_BORDER.getGreen(),
            RomanceColors.TRIGGER_BORDER.getBlue(),
            null);
    float fraction = (Game.time().now() % 100) / 100f;
    hsb[0] = fraction * hsb[0];
    hsb[1] = fraction * hsb[1];
    hsb[2] = fraction * hsb[2];
    return Color.getHSBColor(hsb[0], hsb[1], hsb[2]);
  }
}
