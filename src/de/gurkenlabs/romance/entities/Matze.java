package de.gurkenlabs.romance.entities;

import de.gurkenlabs.litiengine.Align;
import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.AnimationInfo;
import de.gurkenlabs.litiengine.entities.CollisionInfo;
import de.gurkenlabs.litiengine.entities.Creature;
import de.gurkenlabs.litiengine.entities.EntityInfo;
import de.gurkenlabs.litiengine.graphics.animation.IEntityAnimationController;
import de.gurkenlabs.litiengine.input.KeyboardEntityController;
import de.gurkenlabs.romance.GameManager;
import de.gurkenlabs.romance.consts.RomanceSounds;

@EntityInfo(width = 24, height = 24)
@AnimationInfo(spritePrefix = "matze")
@CollisionInfo(
    collision = true,
    collisionBoxWidth = 6,
    collisionBoxHeight = 4,
    align = Align.CENTER)
public class Matze extends Creature {
  public enum MatzeState {
    INTERACTING,
    SMOOCH,
    WALKING
  }

  private static Matze instance;
  private MatzeState state;
  private long lastSmooch;
  private long lastInteract;

  private Matze() {
    super();
    this.setController(IEntityAnimationController.class, new MatzeAnimationController(this));
    getControllers().addController(new KeyboardEntityController<>(this));
    this.state = MatzeState.WALKING;
    movement().onMovementCheck(c -> instance.getState() == MatzeState.WALKING);
  }

  public MatzeState getState() {
    return state;
  }

  public void smooch() {
    this.state = MatzeState.SMOOCH;
    lastSmooch = Game.time().now();
    animations().play("matze-smooch");
    Game.audio().playSound(Game.random().choose(RomanceSounds.smoochSounds), this);
  }

  public long getLastSmooch() {
    return lastSmooch;
  }

  public long getLastInteract() {
    return lastInteract;
  }

  public void interact() {
    this.state = MatzeState.INTERACTING;
    lastInteract = Game.time().now();
    Game.audio().playSound(RomanceSounds.getInteractionSound(GameManager.getObjective()), this);
    Game.audio().playSound(Game.random().choose(RomanceSounds.fulfillSounds), Monna.instance());
  }

  public void walkAroundLikeMotherfucker() {
    this.state = MatzeState.WALKING;
  }

  public static Matze instance() {
    if (instance == null) {
      instance = new Matze();
      instance.setMapId(1);
    }
    return instance;
  }
}
