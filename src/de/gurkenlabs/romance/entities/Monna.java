package de.gurkenlabs.romance.entities;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.AnimationInfo;
import de.gurkenlabs.litiengine.entities.CollisionInfo;
import de.gurkenlabs.litiengine.entities.Creature;
import de.gurkenlabs.litiengine.entities.EntityInfo;
import de.gurkenlabs.litiengine.graphics.animation.IEntityAnimationController;
import de.gurkenlabs.romance.Objective;
import de.gurkenlabs.romance.consts.RomanceSounds;

@EntityInfo(width = 32, height = 24)
@AnimationInfo(spritePrefix = "monna")
@CollisionInfo(collision = true, collisionBoxWidth = 32, collisionBoxHeight = 5)
public class Monna extends Creature {
  private static Monna instance;
  private int currentState;

  private Monna() {
    super();
    setController(IEntityAnimationController.class, new MonnaAnimationController(this));
    this.currentState = 1;
  }

  public static Monna instance() {
    if (instance == null) {
      instance = new Monna();
    }

    return instance;
  }

  public void wave(Objective previous) {
    Game.audio().playSound(Game.random().choose(RomanceSounds.attentionSounds), this);
    switch (previous) {
      case MAKEFIRE:
        animations().setDefault(animations().get("monna-1-wave"));
        break;
      case SERVEWINE:
        animations().setDefault(animations().get("monna-2-wave"));
        break;
      case TURNONTV:
        animations().setDefault(animations().get("monna-3-wave"));
        break;
      case COOK:
        animations().setDefault(animations().get("monna-4-wave"));
        break;
      case SETTABLE:
        animations().setDefault(animations().get("monna-5-wave"));
        break;
      default:
        break;
    }
  }

  public void idle() {
    currentState++;
    animations().setDefault(animations().get(String.format("monna-%d-idle", currentState)));
  }
}
