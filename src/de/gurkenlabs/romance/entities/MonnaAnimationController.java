package de.gurkenlabs.romance.entities;

import de.gurkenlabs.litiengine.graphics.CreatureShadowImageEffect;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.CreatureAnimationController;
import de.gurkenlabs.romance.consts.RomanceColors;

public class MonnaAnimationController extends CreatureAnimationController<Monna> {

  public MonnaAnimationController(Monna creature) {
    super(creature, new Animation("monna-1-idle", true, true));
    addAnimations();
    add(new CreatureShadowImageEffect(creature, RomanceColors.SHADOW));
  }

  private void addAnimations() {
    add(new Animation("monna-1-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-1-idle", true, true));
    add(new Animation("monna-2-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-2-idle", true, true));
    add(new Animation("monna-3-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-3-idle", true, true));
    add(new Animation("monna-4-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-4-idle", true, true));
    add(new Animation("monna-5-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-5-idle", true, true));
    add(new Animation("monna-6-wave", true, false, 120, 120, 120, 120, 300, 120, 120, 120));
    add(new Animation("monna-6-idle", true, true));
  }
}
