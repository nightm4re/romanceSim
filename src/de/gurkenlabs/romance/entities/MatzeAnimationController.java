package de.gurkenlabs.romance.entities;

import de.gurkenlabs.litiengine.graphics.CreatureShadowImageEffect;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.CreatureAnimationController;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.romance.consts.RomanceColors;

public class MatzeAnimationController extends CreatureAnimationController<Matze> {

  public MatzeAnimationController(Matze creature) {
    super(creature, new Animation("matze-idle-down", true, true, 300, 120, 120, 120));
    addAnimations();
    add(new CreatureShadowImageEffect(creature, RomanceColors.SHADOW));
  }

  private void addAnimations() {
    add(new Animation("matze-smooch", false, false, 120, 120, 1000, 120));
    add(
        new Animation(
            "matze-idle-left",
            Resources.spritesheets().get("matze-idle-down"),
            true,
            true,
            300,
            120,
            120,
            12));
    add(
        new Animation(
            "matze-idle-right",
            Resources.spritesheets().get("matze-idle-down"),
            true,
            true,
            300,
            120,
            120,
            12));
  }
}
